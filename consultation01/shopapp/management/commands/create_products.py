from django.core.management import BaseCommand

from shopapp.models import Product


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write("Create products")
        products_names = [
            "Desktop",
            "Laptop",
            "Tablet",
            "Keyboard",
            "Display",
            "Table",
            "Chair",
        ]

        for product_name in products_names:
            product, just_created = Product.objects.get_or_create(name=product_name)
            self.stdout.write(f"A product: {product}")
