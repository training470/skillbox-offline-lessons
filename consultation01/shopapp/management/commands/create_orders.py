from typing import Iterable, Sequence

from django.contrib.auth.models import User
from django.core.management import BaseCommand

from shopapp.models import Order, Product


def chunks(iterable: Sequence, n: int):
    """
    Yield n number of striped chunks from l.
    https://stackoverflow.com/questions/24483182/python-split-list-into-n-chunks
    """
    for i in range(0, n):
        yield iterable[i::n]


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write("Create orders for user")
        user = User.objects.first()
        products = list(Product.objects.all())
        orders_to_create = 2
        product_batches = chunks(products, orders_to_create)
        # create two orders
        for idx, products_batch in enumerate(product_batches, start=1):
            order, created = Order.objects.get_or_create(
                delivery_address=f"delivery_address_{idx}",
                promocode=f"promocode_{idx}",
                user=user,
            )
            order.products.set(products_batch)
            order.save()
            self.stdout.write(f"Created order {order} with products {order.products.all()}")

        self.stdout.write(self.style.SUCCESS("Done"))
